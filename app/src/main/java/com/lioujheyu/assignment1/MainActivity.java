package com.lioujheyu.assignment1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Button;
import android.widget.Toast;

import java.lang.Thread.State;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    final float MIN = 0.0f;
    final float MAX = 1000.0f;
    String[] yAxis = new String[5];

    // GUI components
    private GraphView gView;
    private LinearLayout layoutView;

    private List<Float> value = new ArrayList<>();
    private String curID = "";
    private Random randSeedID;

    private Thread drawThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        for(int i=0; i<5; i++)
            yAxis[i] = ((Float)((MAX - MIN) * (((float)i+1)/5) + MIN)).toString();
        for(int i=0; i<20; i++)
            value.add(0.0f);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        layoutView = (LinearLayout) findViewById(R.id.layout_view);
        Button start = (Button)findViewById(R.id.button_start);
        Button stop = (Button)findViewById(R.id.button_stop);

        start.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                String newID = ((EditText)findViewById(R.id.text_ID)).getText().toString();

                if (newID.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Need a Patient ID !", Toast.LENGTH_SHORT).show();
                    return;
                }

                // First time calling the draw thread
                if (curID.isEmpty()) {
                    drawThread = new GraphViewDrawThread();
                    curID = newID;
                    randSeedID = new Random(Integer.parseInt(curID));
                    drawThread.start();
                    return;
                }

                switch (drawThread.getState()) {
                case TIMED_WAITING: // Trigered by Thread.sleep(). The thread is still running
                    if (!(curID.equals(newID))) {
                        drawThread.interrupt();
                        layoutView.removeAllViews();
                        curID = newID;
                        value.clear();
                        randSeedID = new Random(Integer.parseInt(curID));
                        drawThread = new GraphViewDrawThread();
                        drawThread.start();
                    }
                    // else do nothing but keep thread running
                    break;
                case TERMINATED: // Terminated by stop button
                    if (!(curID.equals(newID))) {
                        value.clear();
                        curID = newID;
                        value.clear();
                        randSeedID = new Random(Integer.parseInt(curID));
                    }
                    drawThread = new GraphViewDrawThread();
                    drawThread.start();
                    break;
                }
            }
        });

        stop.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                layoutView.removeAllViews();
                if (drawThread.isAlive())
                    drawThread.interrupt();
            }
        });
    }

    public class GraphViewDrawThread extends Thread {
        @Override
        public void run() {
            while (true) {
                try {
                    //Background thread cannot update the front UI. runOnUiThread is required to do
                    //so
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            step();
                        }
                    });
                    Thread.sleep(1000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            layoutView.removeAllViews();
                        }
                    });
                }
                catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    return;
                }
            }
        }

        private void step() {
            if (value.size() == 20)
                value.remove(0);
            value.add(randSeedID.nextFloat()*(MAX - MIN) + MIN);

            float[] value_array = new float[value.size()];
            for (int i=0; i<value.size(); i++)
                value_array[i] = value.get(i);

            gView = new GraphView(  getApplicationContext(),
                                    value_array,
                                    "Patient Status",
                                    yAxis,
                                    yAxis,
                                    GraphView.LINE
                                    );

            layoutView.addView(gView);
        }
    }
}
